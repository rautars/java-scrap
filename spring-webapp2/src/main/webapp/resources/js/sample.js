// Javascript code used for demonstration how js event order differs between chrome and firefox.

$(window).on("beforeunload", function(event) {
	event.stopPropagation();
	console.log("beforeunload triggered");
	Cookies.set("simpleval", "simpleVal");
});

$(window).on("customevent", function(event) {
	event.stopPropagation();
	console.log("customevent triggered");
	Cookies.set("simpleval", null);
});

function onComplete(event) {
	event.stopPropagation();
	$(window).trigger("customevent");
}

function ajaxStatus(data) {
	var ajaxStatus = data.status;
	
	switch (ajaxStatus) {
		case "begin":
			break;
		case "complete":
			onComplete(data);
			break;
		case "success":
			break;
	}
}

jsf.ajax.addOnEvent(ajaxStatus);

PrimeFaces.cw("AjaxStatus", "w_ajaxStatus", {id : "ajaxStatus"});
w_ajaxStatus.bindCallback("ajaxComplete", onComplete);

$(window).on('unload', function() {
	console.log("unload triggered");
	Cookies.set("simpleval", null);
});