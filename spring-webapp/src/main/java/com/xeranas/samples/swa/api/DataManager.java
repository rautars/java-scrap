package com.xeranas.samples.swa.api;

import java.util.List;

import com.xeranas.samples.swa.dto.Information;

public interface DataManager {
	
	void saveInfo(Information info);
	
	Information retriveInfo(Long id);
	
	List<Information> retrieveAll();
}
