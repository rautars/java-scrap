package tutor.spring;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class Bookings {
	private final static Logger log = LoggerFactory.getLogger(Bookings.class);
	
	private final static String CREATE_BOOKING_SQL = "insert into BOOKINGS(FIRST_NAME) values (?)";
	private final static String SEARCH_ALL_BOOKINGS_SQL = "select FIRST_NAME from BOOKINGS";
	
    private JdbcTemplate jdbcTemplate;

    public void clearBookings() {
    	jdbcTemplate.execute("delete from BOOKINGS");
    }
    
    public void setupDatabaseSchema() {
		jdbcTemplate.execute("drop table BOOKINGS if exists");
		jdbcTemplate.execute("create table BOOKINGS(ID serial, FIRST_NAME varchar(5) NOT NULL)");
    }
    
    public void book(String... persons) {
        for (String person : persons) {
            log.info("Booking " + person + " in a seat...");
            jdbcTemplate.update(CREATE_BOOKING_SQL, person);
        }
    };
    
    public void bookYetUncommited(String... persons) throws SQLException {
        for (String person : persons) {
        	Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
        	conn.setAutoCommit(false);
    		PreparedStatement ps = conn.prepareStatement(CREATE_BOOKING_SQL);
    		ps.setString(1, person);
    		ps.executeUpdate();
        }
    };
    
    public List<String> findAllBookings() {
        return jdbcTemplate.query(SEARCH_ALL_BOOKINGS_SQL, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("FIRST_NAME");
            }
        });
    }
    
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
}
