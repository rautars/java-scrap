package tutor.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class BookingService {

	private Bookings bookings;

    public void book(String... persons) {
    	bookings.book(persons);
    }
    
    @Transactional
    public void bookTransactional(String... persons) {
    	bookings.book(persons);
    }
    
    @Transactional(readOnly=true)
    public void bookReadOnlyTransactional(String... persons) {
    	bookings.book(persons);
    }

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public void bookFamily(List<String> persons) {
        for (String person : persons) {
        	bookings.book(person);
        }
    }

    public List<String> findAllBookings() {
    	return bookings.findAllBookings();
    }
    
    @Required
	public void setBookings(Bookings bookings) {
		this.bookings = bookings;
	}
    
}
