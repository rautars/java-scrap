package tutor.spring.dao;

import static org.hamcrest.CoreMatchers.equalTo;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import tutor.spring.model.Person;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/beans.xml"})
@Transactional
@Rollback
public class PersonDaoImplTest {

	private PersonDao personDao;
	
	@Test
	public void shouldSavePerson() {
		Person person = new Person();
		person.setFirstName("jonh");
		
		personDao.save(person);
		
		List<Person> persons = personDao.list();
		Assert.assertThat(persons.get(0).getFirstName(), equalTo("jonh"));
	}

	@Autowired
	public void setPersonDao(PersonDao personDao) {
		this.personDao = personDao;
	}
	
	
}
