package tutor.spring;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/beans.xml"})
public class BookingServiceTest {

	private BookingService bookingService;
	private YetAnotherBookingService yetAnotherbookingService;
	private Bookings bookings;
	
	@Before
	public void setUp() {
		bookings.clearBookings();
	}
	
	@Test
	public void testNonTransactionBooking() {
		
		// when
		try {
			bookingService.book("Chris", "Samuel");
		} catch (DataIntegrityViolationException e) {
			// Instead of "expected" data will be verified on BOOKINGS table
		}
			
		// then
		List<String> bookings = bookingService.findAllBookings();
		
		// not transaction book method does not roll back "Chris" even if there was issue when inserting "Samuel"
		Assert.assertThat(bookings.get(0), equalTo("Chris"));
	}

	@Test
	public void testTransactionBooking() {
		
		// when
		try {
			bookingService.bookTransactional("Chris", "Samuel");
		} catch (DataIntegrityViolationException e) {
			// Instead of "expected" data will be verified on BOOKINGS table
		}
			
		// then
		List<String> bookings = bookingService.findAllBookings();
		
		// None should be inserted when exception occurs on "Samuel" record creation 
		Assert.assertThat(bookings.isEmpty(), equalTo(true));
	}
	
	@Test
	public void testReadOnlyTransactionBooking() {
		
		// when
		bookingService.bookReadOnlyTransactional("Chris");
			
		// then
		List<String> bookings = bookingService.findAllBookings();
		
		// Spring transaction "read-only" attribute is only a hint to the provider.
		// In this case it does not work as "Chris" still persisted into database.
		// However if Hibernate would be used as DB provider such record would not be created.
		Assert.assertThat(bookings.get(0), equalTo("Chris"));
	}

	// Isolation level should be used default and should not be changed after application start.
	//
	// http://www.h2database.com/html/advanced.html
	//
	// Please note MVCC is enabled in version 1.4.x by default, when using the MVStore. 
	// In this case, table level locking is not used. 
	// Instead, rows are locked for update, and read committed is used in all cases (changing the isolation level has no effect).
	@Test
	public void testIsolationLvlReadCommited() throws SQLException {
		bookings.bookYetUncommited("Uper");
		
		// then
		List<String> allBookings = bookings.findAllBookings();
		
		Assert.assertThat(allBookings.isEmpty(), equalTo(true));
	}
	
	
	@Test
	public void testPropagationRequiredNew() {
		// given
		Map<String, List<String>> families = new HashMap<String, List<String>>();
		List<String> family1 = new ArrayList<String>();
		family1.add("f1_m1");
		family1.add("f1_m2");
		
		List<String> family2 = new ArrayList<String>();
		family2.add("f2_m1");
		family2.add("f2_member2");
		
		families.put("f1", family1);
		families.put("f2", family2);
		
		// when
		try {
			yetAnotherbookingService.bookFamilies(families);
		} catch (DataIntegrityViolationException e) {
			// Instead of "expected" data will be verified on BOOKINGS table
		}
		
		// then
		List<String> bookings = bookingService.findAllBookings();
		
		// record for family1 was created even if there was issue with second family 
		Assert.assertThat(bookings, hasItem("f1_m1"));
		Assert.assertThat(bookings, hasItem("f1_m2"));
		
		// family2 record was roll backed due invalid name of one family member
		Assert.assertThat(bookings, not(hasItem("f2_m1")));
		Assert.assertThat(bookings, not(hasItem("f2_member2")));
	}
	
	@Autowired
	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}
	
	@Autowired
	public void setYetAnotherbookingService(YetAnotherBookingService yetAnotherbookingService) {
		this.yetAnotherbookingService = yetAnotherbookingService;
	}

	@Autowired
	public void setBookings(Bookings bookings) {
		this.bookings = bookings;
	}

}
