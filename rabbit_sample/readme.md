# RabbitMQ Sample
Simple sample with RabbitMQ lib usage.

## Rabbit Docker
Start rabbit instance: 
```
docker run -d -p 5672:5672 --hostname my-rabbit --name some-rabbit rabbitmq:3
```
