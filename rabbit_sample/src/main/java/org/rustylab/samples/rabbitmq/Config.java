package org.rustylab.samples.rabbitmq;

public class Config {

    public final static String QUEUE_NAME = "some-rabbit";

    private Config() {
    }
}
